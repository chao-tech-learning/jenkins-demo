package com.chao.jenkins.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JenkinsController {

    @GetMapping("/getJenkins")
    public String jenkinsTest() {
        return "Hello Jenkins  V_3.0";
    }
}
